﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kooperativ
{
    public partial class uchet : Form
    {
        public uchet()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ControlBox = false;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form main = new Main();
            main.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form raschet = new raschet();
            raschet.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form pay = new titogo();
            pay.Visible = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form paydor = new paydor();
            paydor.Visible = true; 
        
        }
    }
}
