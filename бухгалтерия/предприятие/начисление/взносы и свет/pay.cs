﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;

using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Globalization;
namespace kooperativ
{
    public partial class titogo : Form
    {
        private MySqlConnection conn;
        private DataTable data;
        private MySqlDataAdapter da;


        private MySqlCommandBuilder cb;
        Props props = new Props();
        public titogo()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ControlBox = false;

            if (conn != null)
                conn.Close();

            string connStr = String.Format("server={0};user id={1}; password={2}; database={3}; CharSet=utf8;pooling=false",
                props.Fields.adress, props.Fields.login, props.Fields.password, props.Fields.dbase);

            try
            {
                conn = new MySqlConnection(connStr);
                conn.Open();

                GetDatabases();



            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error connecting to the server: " + ex.Message);
            }

        }


        private void GetDatabases()
        {


            databaseList.Items.Add(props.Fields.dbase);
            databaseList.SelectedIndex = 0;
            tables.SelectedIndex = 1;
        }




        private void button1_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            for (int i = 1; i < 711; i++)
            {
                string g = comboBox1.Text;
                tabon.Text = Convert.ToDouble(this.tabon.Text).ToString(CultureInfo.InvariantCulture);
                data = new DataTable();
                string x = @"INSERT INTO kosmos.pay (number_boxes,pay,info)  VALUES ('"+i.ToString()+"','-" + tabon.Text + "','абонплата за " + numericUpDown1.Value.ToString() + comboBox1.Text + "')";
                 

                da = new MySqlDataAdapter(x, conn);
                cb = new MySqlCommandBuilder(da);


                da.Fill(data);
                
            }
            MessageBox.Show("абонплата за " + numericUpDown1.Value.ToString() +" "+ comboBox1.Text + " начислена");
            this.Enabled = true;
        }

        private void tables_SelectedIndexChanged(object sender, EventArgs e)
        {
            data = new DataTable();

            da = new MySqlDataAdapter("SELECT * FROM " + tables.SelectedItem.ToString(), conn);
            cb = new MySqlCommandBuilder(da);


            da.Fill(data);

            dataGrid1.DataSource = data;
        }

        private void databaseList_SelectedIndexChanged(object sender, EventArgs e)
        {
            MySqlDataReader reader = null;

            conn.ChangeDatabase(databaseList.SelectedItem.ToString());

            MySqlCommand cmd = new MySqlCommand("SHOW TABLES", conn);
            try
            {
                reader = cmd.ExecuteReader();
                tables.Items.Clear();
                while (reader.Read())
                {
                    tables.Items.Add(reader.GetString(0));
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Failed to populate table list: " + ex.Message);
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }

        private void dataGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (tnumber_box.Text == "")
            {


                MessageBox.Show("Нет данных");

            }

            else
            {
                data = new DataTable();

                da = new MySqlDataAdapter("SELECT schetchik  from kosmos.boxes  WHERE number_boxes='" + tnumber_box.Text.ToString()+"'", conn);
                cb = new MySqlCommandBuilder(da);


                da.Fill(data);

                dataGrid1.DataSource = data;
                if (dataGrid1.Rows[0].Cells[0].Value == null)
                {
                    MessageBox.Show("Нет такого бокса");
                }
                else
                {
                    tschetchik.Text= dataGrid1[0, 0].Value.ToString();
                    button3.Enabled = true;
                }
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Decimal x = Convert.ToDecimal(tschetchik.Text);
           Decimal y = Convert.ToDecimal(tschetchik_new.Text);
            
            //Decimal y = Decimal.Parse(tschetchik_new.Text.Replace(",", CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator), CultureInfo.InvariantCulture);
            Decimal z = y-x;

            //Decimal s = Math.Round((z), 2);
            trashod.Text = z.ToString();
            Decimal i = Convert.ToDecimal(tcenakv.Text);
            Decimal u = z * i;

            //string input = u.ToString();
            ///z = Decimal.Parse(input.Replace(",", "."));
            titog.Text = u.ToString();
            button4.Enabled = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            button4.Enabled = false;
            if (tschetchik_new.Text == null)
            {
                MessageBox.Show("Новые данные счетчика отсутствуют");
                

                 
            }
            titog.Text = Convert.ToDouble(this.titog.Text).ToString(CultureInfo.InvariantCulture);
              data = new DataTable();
              string x = @"UPDATE boxes SET   schetchik='" + tschetchik_new.Text + "' WHERE number_boxes='" + tnumber_box.Text + "'";
          //  MessageBox.Show("" + x);

            da = new MySqlDataAdapter(x, conn);
            cb = new MySqlCommandBuilder(da);


            da.Fill(data);
            data = new DataTable();
            string info = @"расход электричества " + trashod.Text + " по цене:" + tcenakv.Text + "грн. Показания счетчика были" + tschetchik.Text + ". Показания счетчика станут:" + tschetchik_new.Text;
            string y = @"insert kosmos.pay (number_boxes,pay_svet,info_paysvet,info) values ('" + tnumber_box.Text + "','-" + titog.Text + "','"+info.ToString() +"','за свет')";
           // MessageBox.Show("" + y);
          //  MessageBox.Show("?", "Сложный вопрос", MessageBoxButtons.YesNo); 
            da = new MySqlDataAdapter(y, conn);
            cb = new MySqlCommandBuilder(da);


            da.Fill(data);
            button3.Enabled = false;
            button4.Enabled = false;
            MessageBox.Show("Платеж занесен" );

            
        }

        private void button5_Click(object sender, EventArgs e)
        {

          
        }

        

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form uchet = new uchet();
            uchet.Visible = true;
        }
    }
}
