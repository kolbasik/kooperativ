﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net.NetworkInformation;
using System.Diagnostics;
 
using System.Drawing.Imaging;
using System.Drawing.Printing; 

namespace kooperativ
{
    public partial class paydor : Form
    {
        private System.IO.Stream streamToPrint;
        string streamType;

        private PrintDocument printDocument1 = new PrintDocument();
        public paydor()
        {
            InitializeComponent();
            this.TopMost = true;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ControlBox = false;
        }
        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        private static extern bool BitBlt
        (
            IntPtr hdcDest, // handle to destination DC
            int nXDest, // x-coord of destination upper-left corner
            int nYDest, // y-coord of destination upper-left corner
            int nWidth, // width of destination rectangle
            int nHeight, // height of destination rectangle
            IntPtr hdcSrc, // handle to source DC
            int nXSrc, // x-coordinate of source upper-left corner
            int nYSrc, // y-coordinate of source upper-left corner
            System.Int32 dwRop // raster operation code
        );
        private void button1_Click(object sender, EventArgs e)
        {
           // Process.GetCurrentProcess().Kill();
          CaptureScreen();
             
        //   if (printPreviewDialog1.ShowDialog() == DialogResult.OK)
                printdoc1.Print();
            

        }

        private void printDoc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            System.Drawing.Image image = System.Drawing.Image.FromStream(this.streamToPrint);
            int x = e.MarginBounds.X;
            int y = e.MarginBounds.Y;
            int width = image.Width;
            int height = image.Height;
            if ((width / e.MarginBounds.Width) > (height / e.MarginBounds.Height))
            {
                width = e.MarginBounds.Width;
                height = image.Height * e.MarginBounds.Width / image.Width;
            }
            else
            {
                height = e.MarginBounds.Height;
                width = image.Width * e.MarginBounds.Height / image.Height;
            }
            System.Drawing.Rectangle destRect = new System.Drawing.Rectangle(x, y, width, height);
            e.Graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, System.Drawing.GraphicsUnit.Pixel);
        }

        public void StartPrint(Stream streamToPrint, string streamType)
        {
            printdoc1.PrintPage += new PrintPageEventHandler(printDoc_PrintPage);
            
            this.streamToPrint = streamToPrint;
            this.streamType = streamType;
            System.Windows.Forms.PrintDialog PrintDialog1 = new PrintDialog();
            PrintDialog1.AllowSomePages = true;
            PrintDialog1.ShowHelp = true;
            PrintDialog1.Document = printdoc1;

            DialogResult result = PrintDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                printdoc1.Print();
           
                //docToPrint.Print();
            }

        }
        private void CaptureScreen()
        {
            Graphics myGraphics = this.CreateGraphics();
            Size s = this.Size;
            memoryImage = new Bitmap(s.Width, s.Height, myGraphics);
            Graphics memoryGraphics = Graphics.FromImage(memoryImage);
            memoryGraphics.CopyFromScreen(this.Location.X, this.Location.Y, 0, 0, s);
        }
        Bitmap memoryImage;

        private void printDocument1_PrintPage(System.Object sender,
          System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(memoryImage, 0, 0);
        }

        private void contextMenuStrip1_Click(object sender, EventArgs e)
        {
             
        }

        //declare event handler for printing in constructor
        

    //Rest of the code
    Bitmap MemoryImage;
    public void GetPrintArea(Panel pnl)
    {
        MemoryImage = new Bitmap(pnl.Width, pnl.Height);
        Rectangle rect = new Rectangle(0, 0, pnl.Width, pnl.Height);
        pnl.DrawToBitmap(MemoryImage, new Rectangle(0, 0, pnl.Width, pnl.Height));
    }
    protected override void OnPaint(PaintEventArgs e)
    {
        if (MemoryImage != null)
        {
            e.Graphics.DrawImage(MemoryImage, 0, 0);
            base.OnPaint(e);
        }
    }
    void printdoc1_PrintPage(object sender, PrintPageEventArgs e)
    {
        Rectangle pagearea = e.PageBounds;
        e.Graphics.DrawImage(MemoryImage, (pagearea.Width / 2) - (this.panel1.Width / 2), this.panel1.Location.Y);
    }
     

    private void button1_Click_1(object sender, EventArgs e)
    {
        this.Hide();
        Form uchet = new uchet();
        uchet.Visible = true;
    }

    private void button3_Click(object sender, EventArgs e)
    {
        Graphics g1 = this.CreateGraphics();
        Image MyImage = new Bitmap(this.panel1.Width, this.panel1.Height, g1);
        Graphics g2 = Graphics.FromImage(MyImage);
        IntPtr dc1 = g1.GetHdc();
        IntPtr dc2 = g2.GetHdc();
        BitBlt(dc2, 0, 0, this.panel1.Width, this.panel1.Height, dc1, 0, 0, 13369376);
        g1.ReleaseHdc(dc1);
        g2.ReleaseHdc(dc2);
        MyImage.Save(@"c:\PrintPage.jpg", ImageFormat.Jpeg);
        FileStream fileStream = new FileStream(@"c:\PrintPage.jpg", FileMode.Open, FileAccess.Read);
        StartPrint(fileStream, "Image");
        fileStream.Close();

        if (System.IO.File.Exists(@"c:\PrintPage.jpg"))
        {
             System.IO.File.Delete(@"c:\PrintPage.jpg");
        }
    }
    }
}
