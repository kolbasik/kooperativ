﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace kooperativ
{
    public partial class raschet : Form
    {
        private MySqlConnection conn;
        private DataTable data;
        private MySqlDataAdapter da;


        private MySqlCommandBuilder cb;
        Props props = new Props();
        public raschet()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ControlBox = false;

            if (conn != null)
                conn.Close();

            string connStr = String.Format("server={0};user id={1}; password={2}; database={3}; CharSet=utf8;pooling=false",
                props.Fields.adress, props.Fields.login, props.Fields.password, props.Fields.dbase);

            try
            {
                conn = new MySqlConnection(connStr);
                conn.Open();

                GetDatabases();



            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error connecting to the server: " + ex.Message);
            }
            autocout();
        }


        private void autocout()
        {
            data = new DataTable();

            da = new MySqlDataAdapter("select count(number_boxes) from kosmos.boxes;", conn);
            cb = new MySqlCommandBuilder(da);


            da.Fill(data);

            dataGrid1.DataSource = data;
            countboxes.Text = dataGrid1[0, 0].Value.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form uchet = new uchet();
            uchet.Visible = true;
        }
        private void GetDatabases()
        {


            databaseList.Items.Add(props.Fields.dbase);
            databaseList.SelectedIndex = 0;
           // tables.SelectedIndex = 0;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (zatrati.Text == null)
            {
                MessageBox.Show("нет данных");

            }
            else
            {
                Decimal x = Convert.ToDecimal(zatrati.Text);
                Decimal y = Convert.ToDecimal(countboxes.Text);
                Decimal z = x / y;

                Decimal s = Math.Round((z), 2);
                abonplata.Text = s.ToString();
            }
        }

        private void databaseList_SelectedIndexChanged(object sender, EventArgs e)
        {
            MySqlDataReader reader = null;

            conn.ChangeDatabase(databaseList.SelectedItem.ToString());

            MySqlCommand cmd = new MySqlCommand("SHOW TABLES", conn);
            try
            {
                reader = cmd.ExecuteReader();
                tables.Items.Clear();
                while (reader.Read())
                {
                    tables.Items.Add(reader.GetString(0));
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Failed to populate table list: " + ex.Message);
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }

        private void tables_SelectedIndexChanged(object sender, EventArgs e)
        {
            data = new DataTable();

            da = new MySqlDataAdapter("SELECT * FROM " + tables.SelectedItem.ToString(), conn);
            cb = new MySqlCommandBuilder(da);


            da.Fill(data);

            dataGrid1.DataSource = data;
        }

        private void zatrati_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar))
                e.Handled = true;  
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            data = new DataTable();
            string x = @"INSERT INTO pay (number_boxes,pay ,info,pay_svet,info_paysvet)  VALUES ('" + tnb.Text + "','" + tpay.Text + "','" + tinfo.Text + "','" + tsvet.Text + "','" + tsvet_info.Text + "')";

          //  MessageBox.Show("" + x);

            da = new MySqlDataAdapter(x, conn);
            cb = new MySqlCommandBuilder(da);


            da.Fill(data);
            this.Hide();
            Form uchet = new uchet();
            uchet.Visible = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
  if (conn != null)
                conn.Close();
           string x="127.0.0.1";
           string y = "5432";
           string z = "postgres";
           string s = "postgrespass";
           string u = "HoldemManager2";
            string connStr = String.Format("server={0};port={4};user id={1}; password={2}; database={3}; CharSet=utf8;pooling=false",
                x, z, s, u,y);

            try
            {
                conn = new MySqlConnection(connStr);
                conn.Open();

                GetDatabases();



            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error connecting to the server: " + ex.Message);
            }
         
        }
    }
}
