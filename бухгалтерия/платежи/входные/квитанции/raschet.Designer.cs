﻿namespace kooperativ
{
    partial class raschet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(raschet));
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.tsvet = new System.Windows.Forms.TextBox();
            this.tsvet_info = new System.Windows.Forms.RichTextBox();
            this.tinfo = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tpay = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tnb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dataGrid1 = new System.Windows.Forms.DataGridView();
            this.tables = new System.Windows.Forms.ComboBox();
            this.databaseList = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.abonplata = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.zatrati = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.countboxes = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(316, 568);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(132, 44);
            this.button2.TabIndex = 4;
            this.button2.Text = "Назад";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.tsvet);
            this.groupBox1.Controls.Add(this.tsvet_info);
            this.groupBox1.Controls.Add(this.tinfo);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.tpay);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.tnb);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(22, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(426, 369);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Квитанции";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(283, 331);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(110, 23);
            this.button4.TabIndex = 10;
            this.button4.Text = "Занести данные";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // tsvet
            // 
            this.tsvet.Location = new System.Drawing.Point(127, 188);
            this.tsvet.Name = "tsvet";
            this.tsvet.Size = new System.Drawing.Size(266, 20);
            this.tsvet.TabIndex = 9;
            // 
            // tsvet_info
            // 
            this.tsvet_info.Location = new System.Drawing.Point(127, 217);
            this.tsvet_info.Name = "tsvet_info";
            this.tsvet_info.Size = new System.Drawing.Size(266, 89);
            this.tsvet_info.TabIndex = 8;
            this.tsvet_info.Text = "";
            // 
            // tinfo
            // 
            this.tinfo.Location = new System.Drawing.Point(127, 86);
            this.tinfo.Name = "tinfo";
            this.tinfo.Size = new System.Drawing.Size(266, 89);
            this.tinfo.TabIndex = 7;
            this.tinfo.Text = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 220);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Пояснение";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 89);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Пояснение";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 191);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Сумма за свет";
            // 
            // tpay
            // 
            this.tpay.Location = new System.Drawing.Point(127, 59);
            this.tpay.Name = "tpay";
            this.tpay.Size = new System.Drawing.Size(266, 20);
            this.tpay.TabIndex = 3;
            this.tpay.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Сумма за абонплату";
            // 
            // tnb
            // 
            this.tnb.Location = new System.Drawing.Point(127, 29);
            this.tnb.Name = "tnb";
            this.tnb.Size = new System.Drawing.Size(266, 20);
            this.tnb.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Номер бокса";
            // 
            // dataGrid1
            // 
            this.dataGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid1.Location = new System.Drawing.Point(336, 409);
            this.dataGrid1.Name = "dataGrid1";
            this.dataGrid1.Size = new System.Drawing.Size(117, 42);
            this.dataGrid1.TabIndex = 29;
            this.dataGrid1.Visible = false;
            // 
            // tables
            // 
            this.tables.FormattingEnabled = true;
            this.tables.Location = new System.Drawing.Point(171, 403);
            this.tables.Name = "tables";
            this.tables.Size = new System.Drawing.Size(132, 21);
            this.tables.TabIndex = 28;
            this.tables.Visible = false;
            // 
            // databaseList
            // 
            this.databaseList.FormattingEnabled = true;
            this.databaseList.Location = new System.Drawing.Point(44, 403);
            this.databaseList.Name = "databaseList";
            this.databaseList.Size = new System.Drawing.Size(108, 21);
            this.databaseList.TabIndex = 27;
            this.databaseList.Visible = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(336, 496);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(134, 20);
            this.button3.TabIndex = 26;
            this.button3.Text = "Сохранить показания";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            // 
            // abonplata
            // 
            this.abonplata.Location = new System.Drawing.Point(208, 497);
            this.abonplata.Name = "abonplata";
            this.abonplata.Size = new System.Drawing.Size(104, 20);
            this.abonplata.TabIndex = 25;
            this.abonplata.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 504);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(155, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Абоплата в месяц составила";
            this.label4.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(336, 457);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(134, 20);
            this.button1.TabIndex = 23;
            this.button1.Text = "Посчитать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 473);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(289, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "_______________________________________________";
            this.label3.Visible = false;
            // 
            // zatrati
            // 
            this.zatrati.Location = new System.Drawing.Point(208, 457);
            this.zatrati.Name = "zatrati";
            this.zatrati.Size = new System.Drawing.Size(104, 20);
            this.zatrati.TabIndex = 21;
            this.zatrati.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 460);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Общая сумма затрат";
            this.label2.Visible = false;
            // 
            // countboxes
            // 
            this.countboxes.Location = new System.Drawing.Point(208, 430);
            this.countboxes.Name = "countboxes";
            this.countboxes.Size = new System.Drawing.Size(104, 20);
            this.countboxes.TabIndex = 19;
            this.countboxes.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 433);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Количество боксов";
            this.label1.Visible = false;
            // 
            // raschet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 641);
            this.ControlBox = false;
            this.Controls.Add(this.dataGrid1);
            this.Controls.Add(this.tables);
            this.Controls.Add(this.databaseList);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.abonplata);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.zatrati);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.countboxes);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Name = "raschet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tpay;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tnb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox tsvet;
        private System.Windows.Forms.RichTextBox tsvet_info;
        private System.Windows.Forms.RichTextBox tinfo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dataGrid1;
        private System.Windows.Forms.ComboBox tables;
        private System.Windows.Forms.ComboBox databaseList;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox abonplata;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox zatrati;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox countboxes;
        private System.Windows.Forms.Label label1;
    }
}