﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;

using System.Windows.Forms;

namespace kooperativ
{
    public partial class setings : Form
    {

        Props props = new Props(); //экземпляр класса с настройками 
        //Запись настроек
     

        public void SetWriteSettings()
        {
            
             props.Fields.login= textBox1.Text;
             props.Fields.password = maskedTextBox1.Text;
             props.Fields.adress = textBox2.Text;
             props.Fields.dbase = textBox3.Text;
             
             props.WriteXml();
        }

        //Чтение настроек
        private void readSetting()
        {
            
            props.ReadXml();
          //  checkBox1.Checked = props.Fields.autoload;
            // checkBox2.Checked = props.Fields.autohide;
            // checkBox6.Checked = props.Fields.autopost;
            //  tconv.Enabled = props.Fields.autopost;
            textBox1.Text = props.Fields.login;
            maskedTextBox1.Text = props.Fields.password;
            textBox2.Text = props.Fields.adress;
            textBox3.Text = props.Fields.dbase;

        }
         
        public setings()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ControlBox = false;

            readSetting();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ControlBox = false;

        }

        private void button1_Click(object sender, EventArgs e)
        {

            SetWriteSettings();     
            this.Hide();
            Form main = new Main();
            main.Visible = true;
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form main = new Main();
            main.Visible = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}
