﻿namespace kooperativ
{
    partial class showbox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(showbox));
            this.databaseList = new System.Windows.Forms.ComboBox();
            this.tables = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.dataGrid1 = new System.Windows.Forms.DataGridView();
            this.tadress_arenda = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ttel_arenda = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.totch_arenda = new System.Windows.Forms.TextBox();
            this.timia_arenda = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tfamilia_arenda = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tadress_hoz = new System.Windows.Forms.RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ttel_hoz = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tgov_number = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tmodel_avto = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.totch_hoz = new System.Windows.Forms.TextBox();
            this.timia_hoz = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tFamilia_hoz = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tnumber_boxes = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.dataGrid2 = new System.Windows.Forms.DataGridView();
            this.tables2 = new System.Windows.Forms.ComboBox();
            this.databaseList2 = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid2)).BeginInit();
            this.SuspendLayout();
            // 
            // databaseList
            // 
            this.databaseList.FormattingEnabled = true;
            this.databaseList.Location = new System.Drawing.Point(361, 448);
            this.databaseList.Name = "databaseList";
            this.databaseList.Size = new System.Drawing.Size(65, 21);
            this.databaseList.TabIndex = 0;
            this.databaseList.Visible = false;
            this.databaseList.SelectedIndexChanged += new System.EventHandler(this.databaseList_SelectedIndexChanged_1);
            // 
            // tables
            // 
            this.tables.FormattingEnabled = true;
            this.tables.Location = new System.Drawing.Point(319, 448);
            this.tables.Name = "tables";
            this.tables.Size = new System.Drawing.Size(71, 21);
            this.tables.TabIndex = 1;
            this.tables.Visible = false;
            this.tables.SelectedIndexChanged += new System.EventHandler(this.tables_SelectedIndexChanged_1);
            // 
            // button3
            // 
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(323, 9);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(119, 26);
            this.button3.TabIndex = 4;
            this.button3.Text = "Просмотр";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dataGrid1
            // 
            this.dataGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid1.Location = new System.Drawing.Point(302, 448);
            this.dataGrid1.Name = "dataGrid1";
            this.dataGrid1.Size = new System.Drawing.Size(46, 18);
            this.dataGrid1.TabIndex = 5;
            this.dataGrid1.Visible = false;
            // 
            // tadress_arenda
            // 
            this.tadress_arenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tadress_arenda.Location = new System.Drawing.Point(106, 392);
            this.tadress_arenda.Name = "tadress_arenda";
            this.tadress_arenda.Size = new System.Drawing.Size(371, 42);
            this.tadress_arenda.TabIndex = 63;
            this.tadress_arenda.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 401);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 62;
            this.label10.Text = "Адрес";
            // 
            // ttel_arenda
            // 
            this.ttel_arenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ttel_arenda.Location = new System.Drawing.Point(106, 366);
            this.ttel_arenda.Name = "ttel_arenda";
            this.ttel_arenda.Size = new System.Drawing.Size(371, 26);
            this.ttel_arenda.TabIndex = 61;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 373);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 60;
            this.label11.Text = "Телефон";
            // 
            // totch_arenda
            // 
            this.totch_arenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.totch_arenda.Location = new System.Drawing.Point(106, 340);
            this.totch_arenda.Name = "totch_arenda";
            this.totch_arenda.Size = new System.Drawing.Size(371, 26);
            this.totch_arenda.TabIndex = 59;
            // 
            // timia_arenda
            // 
            this.timia_arenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.timia_arenda.Location = new System.Drawing.Point(106, 317);
            this.timia_arenda.Name = "timia_arenda";
            this.timia_arenda.Size = new System.Drawing.Size(371, 26);
            this.timia_arenda.TabIndex = 58;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 347);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 13);
            this.label12.TabIndex = 57;
            this.label12.Text = "Отчество";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 324);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 56;
            this.label13.Text = "Имя";
            // 
            // tfamilia_arenda
            // 
            this.tfamilia_arenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tfamilia_arenda.Location = new System.Drawing.Point(106, 294);
            this.tfamilia_arenda.Name = "tfamilia_arenda";
            this.tfamilia_arenda.Size = new System.Drawing.Size(371, 26);
            this.tfamilia_arenda.TabIndex = 55;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 301);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 13);
            this.label14.TabIndex = 54;
            this.label14.Text = "Фамилия";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 414);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 13);
            this.label9.TabIndex = 49;
            // 
            // tadress_hoz
            // 
            this.tadress_hoz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tadress_hoz.Location = new System.Drawing.Point(106, 196);
            this.tadress_hoz.Name = "tadress_hoz";
            this.tadress_hoz.Size = new System.Drawing.Size(371, 56);
            this.tadress_hoz.TabIndex = 48;
            this.tadress_hoz.Text = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 200);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 47;
            this.label8.Text = "Адрес";
            // 
            // ttel_hoz
            // 
            this.ttel_hoz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ttel_hoz.Location = new System.Drawing.Point(106, 170);
            this.ttel_hoz.Name = "ttel_hoz";
            this.ttel_hoz.Size = new System.Drawing.Size(371, 26);
            this.ttel_hoz.TabIndex = 46;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 177);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 45;
            this.label7.Text = "Телефон";
            // 
            // tgov_number
            // 
            this.tgov_number.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tgov_number.Location = new System.Drawing.Point(106, 148);
            this.tgov_number.Name = "tgov_number";
            this.tgov_number.Size = new System.Drawing.Size(371, 26);
            this.tgov_number.TabIndex = 44;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 43;
            this.label6.Text = "Гос. номер";
            // 
            // tmodel_avto
            // 
            this.tmodel_avto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tmodel_avto.Location = new System.Drawing.Point(106, 126);
            this.tmodel_avto.Name = "tmodel_avto";
            this.tmodel_avto.Size = new System.Drawing.Size(371, 26);
            this.tmodel_avto.TabIndex = 42;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 133);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 41;
            this.label5.Text = "Марка машины";
            // 
            // totch_hoz
            // 
            this.totch_hoz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.totch_hoz.Location = new System.Drawing.Point(106, 100);
            this.totch_hoz.Name = "totch_hoz";
            this.totch_hoz.Size = new System.Drawing.Size(371, 26);
            this.totch_hoz.TabIndex = 40;
            // 
            // timia_hoz
            // 
            this.timia_hoz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.timia_hoz.Location = new System.Drawing.Point(106, 77);
            this.timia_hoz.Name = "timia_hoz";
            this.timia_hoz.Size = new System.Drawing.Size(371, 26);
            this.timia_hoz.TabIndex = 39;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 38;
            this.label4.Text = "Отчество";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 37;
            this.label3.Text = "Имя";
            // 
            // tFamilia_hoz
            // 
            this.tFamilia_hoz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tFamilia_hoz.Location = new System.Drawing.Point(106, 54);
            this.tFamilia_hoz.Name = "tFamilia_hoz";
            this.tFamilia_hoz.Size = new System.Drawing.Size(371, 26);
            this.tFamilia_hoz.TabIndex = 36;
            this.tFamilia_hoz.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tFamilia_hoz_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "Фамилия";
            // 
            // tnumber_boxes
            // 
            this.tnumber_boxes.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tnumber_boxes.Location = new System.Drawing.Point(106, 9);
            this.tnumber_boxes.Name = "tnumber_boxes";
            this.tnumber_boxes.Size = new System.Drawing.Size(149, 24);
            this.tnumber_boxes.TabIndex = 33;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(9, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 15);
            this.label1.TabIndex = 32;
            this.label1.Text = "Номер бокса";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(345, 545);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(132, 44);
            this.button2.TabIndex = 64;
            this.button2.Text = "Назад";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 269);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(97, 13);
            this.label15.TabIndex = 65;
            this.label15.Text = "Доверенное лицо";
            // 
            // dataGrid2
            // 
            this.dataGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid2.Location = new System.Drawing.Point(294, 475);
            this.dataGrid2.Name = "dataGrid2";
            this.dataGrid2.Size = new System.Drawing.Size(32, 17);
            this.dataGrid2.TabIndex = 68;
            this.dataGrid2.Visible = false;
            // 
            // tables2
            // 
            this.tables2.FormattingEnabled = true;
            this.tables2.Location = new System.Drawing.Point(332, 475);
            this.tables2.Name = "tables2";
            this.tables2.Size = new System.Drawing.Size(23, 21);
            this.tables2.TabIndex = 67;
            this.tables2.Visible = false;
            this.tables2.SelectedIndexChanged += new System.EventHandler(this.tables2_SelectedIndexChanged);
            // 
            // databaseList2
            // 
            this.databaseList2.FormattingEnabled = true;
            this.databaseList2.Location = new System.Drawing.Point(369, 475);
            this.databaseList2.Name = "databaseList2";
            this.databaseList2.Size = new System.Drawing.Size(22, 21);
            this.databaseList2.TabIndex = 66;
            this.databaseList2.Visible = false;
            this.databaseList2.SelectedIndexChanged += new System.EventHandler(this.databaseList2_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(19, 448);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(115, 13);
            this.label16.TabIndex = 69;
            this.label16.Text = "Долг по кооперативу";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(20, 471);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 13);
            this.label17.TabIndex = 70;
            this.label17.Text = "Долг за свет";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(20, 495);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(211, 13);
            this.label18.TabIndex = 71;
            this.label18.Text = "__________________________________";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(19, 518);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(59, 20);
            this.label19.TabIndex = 72;
            this.label19.Text = "Итого";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.textBox1.Location = new System.Drawing.Point(140, 512);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(87, 26);
            this.textBox1.TabIndex = 73;
            this.textBox1.Text = "0";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(140, 445);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(87, 20);
            this.textBox2.TabIndex = 74;
            this.textBox2.Text = "0";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(140, 476);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(87, 20);
            this.textBox3.TabIndex = 75;
            this.textBox3.Text = "0";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(207, 545);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 44);
            this.button1.TabIndex = 76;
            this.button1.Text = "Распечатать квитанцию";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // showbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 610);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.dataGrid2);
            this.Controls.Add(this.tables2);
            this.Controls.Add(this.databaseList2);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tadress_arenda);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.ttel_arenda);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.totch_arenda);
            this.Controls.Add(this.timia_arenda);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tfamilia_arenda);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tadress_hoz);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.ttel_hoz);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tgov_number);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tmodel_avto);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.totch_hoz);
            this.Controls.Add(this.timia_hoz);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tFamilia_hoz);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tnumber_boxes);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGrid1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.tables);
            this.Controls.Add(this.databaseList);
            this.Name = "showbox";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox databaseList;
        private System.Windows.Forms.ComboBox tables;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridView dataGrid1;
        private System.Windows.Forms.RichTextBox tadress_arenda;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox ttel_arenda;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox totch_arenda;
        private System.Windows.Forms.TextBox timia_arenda;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tfamilia_arenda;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RichTextBox tadress_hoz;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox ttel_hoz;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tgov_number;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tmodel_avto;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox totch_hoz;
        private System.Windows.Forms.TextBox timia_hoz;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tFamilia_hoz;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tnumber_boxes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView dataGrid2;
        private System.Windows.Forms.ComboBox tables2;
        private System.Windows.Forms.ComboBox databaseList2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button button1;
         
        
    }
}