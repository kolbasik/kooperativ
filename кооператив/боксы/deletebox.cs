﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace kooperativ
{
    public partial class deletebox : Form
    {
        private MySqlConnection conn;
        private DataTable data;
        private MySqlDataAdapter da;


        private MySqlCommandBuilder cb;
        Props props = new Props();
        public deletebox()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ControlBox = false;

            if (conn != null)
                conn.Close();

            string connStr = String.Format("server={0};user id={1}; password={2}; database={3}; CharSet=utf8;pooling=false",
                props.Fields.adress, props.Fields.login, props.Fields.password, props.Fields.dbase);

            try
            {
                conn = new MySqlConnection(connStr);
                conn.Open();

                GetDatabases();



            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error connecting to the server: " + ex.Message);
            }
            
        }
     private void GetDatabases()
        {


            databaseList.Items.Add(props.Fields.dbase);
            databaseList.SelectedIndex= 0;
            tables.SelectedIndex = 0;
        }
        private void button1_Click(object sender, EventArgs e)
        {
       // delete from kosmos.boxes where number_boxes=44

            data = new DataTable();
            string x = @"delete from boxes where number_boxes='"+textBox1.Text+"'";

            

            da = new MySqlDataAdapter(x, conn);
            cb = new MySqlCommandBuilder(da);


            da.Fill(data);
            this.Hide();
            Form showkooperativ = new showkooperativ();
            showkooperativ.Visible = true;
        }
       

      

        private void tables_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            data = new DataTable();

            da = new MySqlDataAdapter("SELECT * FROM " + tables.SelectedItem.ToString(), conn);
            cb = new MySqlCommandBuilder(da);


            da.Fill(data);

            dataGrid1.DataSource = data;
        }

        private void databaseList_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            MySqlDataReader reader = null;

            conn.ChangeDatabase(databaseList.SelectedItem.ToString());

            MySqlCommand cmd = new MySqlCommand("SHOW TABLES", conn);
            try
            {
                reader = cmd.ExecuteReader();
                tables.Items.Clear();
                while (reader.Read())
                {
                    tables.Items.Add(reader.GetString(0));
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Failed to populate table list: " + ex.Message);
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar))
                e.Handled = true;  
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form showkooperativ = new showkooperativ();
            showkooperativ.Visible = true;
        }
    }
}

