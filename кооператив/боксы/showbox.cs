﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kooperativ
{
    public partial class showbox : Form
    {
        private MySqlConnection conn, conn1;
        private DataTable data;
        private MySqlDataAdapter da;
    
     
        private MySqlCommandBuilder cb;
        Props props = new Props();
        public showbox()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ControlBox = false;

            if (conn != null)
                conn.Close();

            string connStr = String.Format("server={0};user id={1}; password={2}; database={3}; CharSet=utf8;pooling=false",
                props.Fields.adress, props.Fields.login, props.Fields.password, props.Fields.dbase);

            try
            {
                conn = new MySqlConnection(connStr);
                conn.Open();
                conn1 = new MySqlConnection(connStr);
                conn1.Open();

                GetDatabases();



            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error connecting to the server: " + ex.Message);
            }

        }

      
        private void GetDatabases()
        {
          

            databaseList.Items.Add(props.Fields.dbase);
            databaseList.SelectedIndex = 0;
            tables.SelectedIndex = 0;
            databaseList2.Items.Add(props.Fields.dbase);
            databaseList2.SelectedIndex = 0;
            tables2.SelectedIndex = 1;
           
        }
        private void databaseList_SelectedIndexChanged(object sender, System.EventArgs e)
        {

        }

        

        private void databaseList_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            MySqlDataReader reader = null;

            conn.ChangeDatabase(databaseList.SelectedItem.ToString());

            MySqlCommand cmd = new MySqlCommand("SHOW TABLES", conn);
            try
            {
                reader = cmd.ExecuteReader();
                tables.Items.Clear();
                while (reader.Read())
                {
                    tables.Items.Add(reader.GetString(0));
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Failed to populate table list: " + ex.Message);
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }

        private void tables_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            data = new DataTable();

            da = new MySqlDataAdapter("SELECT * FROM " + tables.SelectedItem.ToString(), conn1);
            cb = new MySqlCommandBuilder(da);

            
            da.Fill(data);

            dataGrid1.DataSource = data;
        }

        


       

       

        private void button3_Click(object sender, EventArgs e)
        {
            if (tnumber_boxes.Text == "")
            {


                MessageBox.Show("Нет данных");

            }

            else
            {
                data = new DataTable();

                da = new MySqlDataAdapter("SELECT number_boxes,Familia_hoz,imia_hoz,otch_hoz, model_avto, gov_number,tel_hoz, adress_hoz, familia_arenda, imia_arenda, otch_arenda, tel_arenda, adress_arenda FROM  kosmos.boxes  WHERE number_boxes='" + tnumber_boxes.Text+"'", conn);
                cb = new MySqlCommandBuilder(da);


                da.Fill(data);

                dataGrid1.DataSource = data;

                if (dataGrid1.Rows[0].Cells[0].Value == null)
                {
                    MessageBox.Show("Нет такого бокса");
                }
                else
                {

                    tnumber_boxes.Text = dataGrid1[0, 0].Value.ToString();
                    tFamilia_hoz.Text = dataGrid1[1, 0].Value.ToString();
                    timia_hoz.Text = dataGrid1[2, 0].Value.ToString();
                    totch_hoz.Text = dataGrid1[3, 0].Value.ToString();
                    tmodel_avto.Text = dataGrid1[4, 0].Value.ToString();
                    tgov_number.Text = dataGrid1[5, 0].Value.ToString();
                    ttel_hoz.Text = dataGrid1[6, 0].Value.ToString();
                    tadress_hoz.Text = dataGrid1[7, 0].Value.ToString();
                    tfamilia_arenda.Text = dataGrid1[8, 0].Value.ToString();
                    timia_arenda.Text = dataGrid1[9, 0].Value.ToString();
                    totch_arenda.Text = dataGrid1[10, 0].Value.ToString();
                    ttel_arenda.Text = dataGrid1[11, 0].Value.ToString();
                    tadress_arenda.Text = dataGrid1[12, 0].Value.ToString();

                }
                ///=================================
                data = new DataTable();

                da = new MySqlDataAdapter("SELECT  sum(pay) FROM pay  where number_boxes='" + tnumber_boxes.Text.ToString() + "'", conn1);
                cb = new MySqlCommandBuilder(da);


                da.Fill(data);

                dataGrid2.DataSource = data;

                if (dataGrid2.Rows[0].Cells[0].Value == null)
                {
                    MessageBox.Show("Нет такого бокса");
                }
                else
                {

                    textBox2.Text = dataGrid2[0, 0].Value.ToString();


                }
                data = new DataTable();
                
                da = new MySqlDataAdapter("SELECT  sum(pay_svet) FROM pay  where number_boxes='" + tnumber_boxes.Text.ToString()+"'", conn1);
                cb = new MySqlCommandBuilder(da);


                 da.Fill(data);
               dataGrid2.DataSource = data;
               textBox3.Text = dataGrid2[0, 0].Value.ToString();
               if (textBox2.Text == "")

               { textBox2.Text = Convert.ToString("0"); }
               else
               { Decimal x = Convert.ToDecimal(textBox2.Text);

               Decimal k= Math.Round((x), 2);
               textBox2.Text = k.ToString();
               }

               Decimal n = Convert.ToDecimal(textBox2.Text);

               if (textBox3.Text == "")

               { textBox3.Text = Convert.ToString("0"); }
               else
               { Decimal y = Convert.ToDecimal(textBox3.Text);
               Decimal l = Math.Round((y), 2);
               textBox3.Text = l.ToString();
               
               }

               Decimal h = Convert.ToDecimal(textBox3.Text);
               Decimal z = n + h;
               Decimal d = Math.Round((z), 2);
               textBox1.Text = d.ToString();

              



            }   
            
     

        }

        private void button6_Click(object sender, EventArgs e)
        {
            dataGrid1.Columns.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            data = new DataTable();
            string x = @"INSERT INTO kosmos.boxes (number_boxes, Familia_hoz, imia_hoz, otch_hoz, model_avto, gov_number, tel_hoz, adress_hoz, familia_arenda, imia_arenda, otch_arenda, tel_arenda, adress_arenda)  VALUES ('" + tnumber_boxes.Text + "','" + tFamilia_hoz.Text + "','"+timia_hoz.Text+"','" + totch_hoz.Text + "','" + tmodel_avto.Text + "','" + tgov_number.Text + "','" + ttel_hoz.Text + "','" + tadress_hoz.Text + "','" + tfamilia_arenda.Text + "','" + timia_arenda.Text + "','" + totch_arenda.Text + "','" + ttel_arenda.Text + "','" + tadress_arenda.Text + "')";

           // MessageBox.Show("" + x);
            
            da = new MySqlDataAdapter(x, conn);
            cb = new MySqlCommandBuilder(da);


            da.Fill(data);
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            Form showkooperativ = new showkooperativ();
            showkooperativ.Visible = true;

        }

        private void tFamilia_hoz_KeyPress(object sender, KeyPressEventArgs e)
        {
              
        }

         

        

        private void tables2_SelectedIndexChanged(object sender, EventArgs e)
        {
            data = new DataTable();

            da = new MySqlDataAdapter("SELECT * FROM " + tables2.SelectedItem.ToString(), conn1);
            cb = new MySqlCommandBuilder(da);


            da.Fill(data);

            dataGrid2.DataSource = data;
        }

        private void databaseList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            MySqlDataReader reader = null;

            conn.ChangeDatabase(databaseList2.SelectedItem.ToString());

            MySqlCommand cmd = new MySqlCommand("SHOW TABLES", conn1);
            try
            {
                reader = cmd.ExecuteReader();
                tables2.Items.Clear();
                while (reader.Read())
                {
                    tables2.Items.Add(reader.GetString(0));
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Failed to populate table list: " + ex.Message);
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
             this.Hide();
             Form quittance = new quittance();
             quittance.Visible = true;
             quittance.TopMost = true;
        }

     
 

        
    }
}

