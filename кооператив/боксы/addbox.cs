﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kooperativ
{
    public partial class addbox : Form
    {
        private MySqlConnection conn;
        private DataTable data;
        private MySqlDataAdapter da;


        private MySqlCommandBuilder cb;
        Props props = new Props();
        public addbox()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ControlBox = false;

           
            
            string connStr = String.Format("server={0};user id={1}; password={2}; database={3}; CharSet=utf8;pooling=false",
                props.Fields.adress, props.Fields.login, props.Fields.password, props.Fields.dbase);

            try
            {
                conn = new MySqlConnection(connStr);
                conn.Open();
            }
      
          
            catch (MySqlException ex)
            {
                MessageBox.Show("Error connecting to the server: " + ex.Message);
            }

        }
        private void button4_Click(object sender, EventArgs e)
        {

            this.Hide();
            Form showkooperativ = new showkooperativ();
           showkooperativ.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {

            data = new DataTable();
            string x = @"INSERT INTO boxes (number_boxes, Familia_hoz, imia_hoz, otch_hoz, model_avto, gov_number, tel_hoz, adress_hoz, familia_arenda, imia_arenda, otch_arenda, tel_arenda, adress_arenda)  VALUES ('" + tnumber_boxes.Text + "','" + tFamilia_hoz.Text + "','" + timia_hoz.Text + "','" + totch_hoz.Text + "','" + tmodel_avto.Text + "','" + tgov_number.Text + "','" + ttel_hoz.Text + "','" + tadress_hoz.Text + "','" + tfamilia_arenda.Text + "','" + timia_arenda.Text + "','" + totch_arenda.Text + "','" + ttel_arenda.Text + "','" + tadress_arenda.Text + "')";

           // MessageBox.Show("" + x);

            da = new MySqlDataAdapter(x, conn);
            cb = new MySqlCommandBuilder(da);


            da.Fill(data);

            this.Hide();
            Form showkooperativ = new showkooperativ();
            showkooperativ.Visible = true;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                tfamilia_arenda.Enabled = true;
                timia_arenda.Enabled = true;
                totch_arenda.Enabled = true;
                ttel_arenda.Enabled = true;
                tadress_arenda.Enabled = true;

            }
            else
            {
                tfamilia_arenda.Enabled = false;
                tfamilia_arenda.Clear();
                timia_arenda.Enabled = false;
                timia_arenda.Clear();
                totch_arenda.Enabled = false;
                totch_arenda.Clear();
                ttel_arenda.Enabled = false;
                ttel_arenda.Clear();
                tadress_arenda.Enabled = false;
                tadress_arenda.Clear();
            }
     
            }

      
    }
}
