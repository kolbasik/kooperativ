﻿namespace kooperativ
{
    partial class addbox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(addbox));
            this.label1 = new System.Windows.Forms.Label();
            this.tnumber_boxes = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tFamilia_hoz = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.timia_hoz = new System.Windows.Forms.TextBox();
            this.totch_hoz = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tmodel_avto = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tgov_number = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ttel_hoz = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tadress_hoz = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.tadress_arenda = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ttel_arenda = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.totch_arenda = new System.Windows.Forms.TextBox();
            this.timia_arenda = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tfamilia_arenda = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Номер бокса";
            // 
            // tnumber_boxes
            // 
            this.tnumber_boxes.Location = new System.Drawing.Point(127, 54);
            this.tnumber_boxes.Name = "tnumber_boxes";
            this.tnumber_boxes.Size = new System.Drawing.Size(337, 20);
            this.tnumber_boxes.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Фамилия";
            // 
            // tFamilia_hoz
            // 
            this.tFamilia_hoz.Location = new System.Drawing.Point(127, 84);
            this.tFamilia_hoz.Name = "tFamilia_hoz";
            this.tFamilia_hoz.Size = new System.Drawing.Size(337, 20);
            this.tFamilia_hoz.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Имя";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Отчество";
            // 
            // timia_hoz
            // 
            this.timia_hoz.Location = new System.Drawing.Point(127, 107);
            this.timia_hoz.Name = "timia_hoz";
            this.timia_hoz.Size = new System.Drawing.Size(337, 20);
            this.timia_hoz.TabIndex = 7;
            // 
            // totch_hoz
            // 
            this.totch_hoz.Location = new System.Drawing.Point(127, 130);
            this.totch_hoz.Name = "totch_hoz";
            this.totch_hoz.Size = new System.Drawing.Size(337, 20);
            this.totch_hoz.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 163);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Марка машины";
            // 
            // tmodel_avto
            // 
            this.tmodel_avto.Location = new System.Drawing.Point(127, 156);
            this.tmodel_avto.Name = "tmodel_avto";
            this.tmodel_avto.Size = new System.Drawing.Size(337, 20);
            this.tmodel_avto.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 185);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Гос. номер";
            // 
            // tgov_number
            // 
            this.tgov_number.Location = new System.Drawing.Point(127, 178);
            this.tgov_number.Name = "tgov_number";
            this.tgov_number.Size = new System.Drawing.Size(337, 20);
            this.tgov_number.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 207);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Телефон";
            // 
            // ttel_hoz
            // 
            this.ttel_hoz.Location = new System.Drawing.Point(127, 200);
            this.ttel_hoz.Name = "ttel_hoz";
            this.ttel_hoz.Size = new System.Drawing.Size(337, 20);
            this.ttel_hoz.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(30, 230);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Адрес";
            // 
            // tadress_hoz
            // 
            this.tadress_hoz.Location = new System.Drawing.Point(127, 226);
            this.tadress_hoz.Name = "tadress_hoz";
            this.tadress_hoz.Size = new System.Drawing.Size(337, 56);
            this.tadress_hoz.TabIndex = 16;
            this.tadress_hoz.Text = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(33, 321);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 13);
            this.label9.TabIndex = 17;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(33, 301);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(161, 17);
            this.checkBox1.TabIndex = 18;
            this.checkBox1.Text = "Есть ли доверенное лицо?";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // tadress_arenda
            // 
            this.tadress_arenda.Enabled = false;
            this.tadress_arenda.Location = new System.Drawing.Point(127, 422);
            this.tadress_arenda.Name = "tadress_arenda";
            this.tadress_arenda.Size = new System.Drawing.Size(337, 42);
            this.tadress_arenda.TabIndex = 31;
            this.tadress_arenda.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(33, 431);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "Адрес";
            // 
            // ttel_arenda
            // 
            this.ttel_arenda.Enabled = false;
            this.ttel_arenda.Location = new System.Drawing.Point(127, 396);
            this.ttel_arenda.Name = "ttel_arenda";
            this.ttel_arenda.Size = new System.Drawing.Size(337, 20);
            this.ttel_arenda.TabIndex = 29;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(30, 403);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "Телефон";
            // 
            // totch_arenda
            // 
            this.totch_arenda.Enabled = false;
            this.totch_arenda.Location = new System.Drawing.Point(127, 370);
            this.totch_arenda.Name = "totch_arenda";
            this.totch_arenda.Size = new System.Drawing.Size(337, 20);
            this.totch_arenda.TabIndex = 27;
            // 
            // timia_arenda
            // 
            this.timia_arenda.Enabled = false;
            this.timia_arenda.Location = new System.Drawing.Point(127, 347);
            this.timia_arenda.Name = "timia_arenda";
            this.timia_arenda.Size = new System.Drawing.Size(337, 20);
            this.timia_arenda.TabIndex = 26;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(30, 377);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 13);
            this.label12.TabIndex = 25;
            this.label12.Text = "Отчество";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(30, 354);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Имя";
            // 
            // tfamilia_arenda
            // 
            this.tfamilia_arenda.Enabled = false;
            this.tfamilia_arenda.Location = new System.Drawing.Point(127, 324);
            this.tfamilia_arenda.Name = "tfamilia_arenda";
            this.tfamilia_arenda.Size = new System.Drawing.Size(337, 20);
            this.tfamilia_arenda.TabIndex = 23;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(30, 331);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "Фамилия";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(33, 562);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(207, 36);
            this.button3.TabIndex = 32;
            this.button3.Text = "Добавить в базу";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(341, 562);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(123, 36);
            this.button4.TabIndex = 33;
            this.button4.Text = "Назад";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // addbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 629);
            this.ControlBox = false;
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.tadress_arenda);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.ttel_arenda);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.totch_arenda);
            this.Controls.Add(this.timia_arenda);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tfamilia_arenda);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tadress_hoz);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.ttel_hoz);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tgov_number);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tmodel_avto);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.totch_hoz);
            this.Controls.Add(this.timia_hoz);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tFamilia_hoz);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tnumber_boxes);
            this.Controls.Add(this.label1);
            this.Name = "addbox";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tnumber_boxes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tFamilia_hoz;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox timia_hoz;
        private System.Windows.Forms.TextBox totch_hoz;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tmodel_avto;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tgov_number;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox ttel_hoz;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RichTextBox tadress_hoz;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.RichTextBox tadress_arenda;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox ttel_arenda;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox totch_arenda;
        private System.Windows.Forms.TextBox timia_arenda;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tfamilia_arenda;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}